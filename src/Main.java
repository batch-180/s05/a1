public class Main {
    public static void main(String[] args) {


        User newUser = new User("Craig","Guarino",20,"Bulacan");
        System.out.println("User's first name:");
        System.out.println(newUser.getFirstName());
        System.out.println("User's last name:");
        System.out.println(newUser.getLastName());
        System.out.println("User's age:");
        System.out.println(newUser.getAge());
        System.out.println("User's address:");
        System.out.println(newUser.getAddress());


        Course newCourse = new Course();
        newCourse.setName("JAVA");
        newCourse.setDescription("Introduction to JAVA");
        newCourse.setFee(5000);
        newCourse.setSeats(30);
        newCourse.setInstructor("Tee Jae");

        System.out.println("Course's name:");
        System.out.println(newCourse.getName());
        System.out.println("Course's Description:");
        System.out.println(newCourse.getDescription());
        System.out.println("Course's seats:");
        System.out.println(newCourse.getSeats());
        System.out.println("Course's instructor's first name:");
        System.out.println(newCourse.getInstructor());




    }
}